# Asteroids API

---

## Table Of Content

### Description:

Tool or API around [NASA Asteroids API](https://api.nasa.gov), specifically - _Asteroids - NeoWs_ RESTful web service.

### Purpose:

Receive information about Asteroids near Earth, according to requirements (see below).

### Requirements:

1. Show 10 asteroids that passed the closest to Earth between two user-provided dates.
2. Show characteristics of the largest asteroid (estimated diameter) passing Earth during a user-provided year.

_* Indirect requirements - caching and persisting data to avoid extra external API calls_

### API restrictions:

Due to the limitations on external [NASA Asteroids API](https://api.nasa.gov) there are restrictions of Asteroids REST
API tool:

- User-provided dates - only 7-days range allowed
- In case of downtime - only cached data can be served

## HOW-TOs

### Pre-requisitions:

- Git
- Maven
- JVM (Java 11+)

### Build & run:

1. Clone this repo: `https://gitlab.com/Taraskin/asteroid-api.git`
2. Navigate to source code: `cd asteroid-api`
3. Run maven build: `mvn clean install`
4. Run java app: `java -jar target/asteroid-api-0.0.1-SNAPSHOT.jar`

### Notes:

> * _For data caching **H2** in-memory database is used by default. Update DB configuration settings in `application.yml` file to change it_
>
>* _Some configuration settings set to their defaults (see below)_
>
>* _To use the API tool [NASA Asteroids API](https://api.nasa.gov) -API key might be required (need free registration). **DEMO_KEY** is used by default (has limited usage)_

### Environment variables & configuration defaults:

All application configuration settings loacated `application.yml` and can be updated by using environmental variables.

|    Variable     |        Type         |               Default value                |              Description               |
|:---------------:|:-------------------:|:------------------------------------------:|:--------------------------------------:|
|     DB_URL      |  JDBC URL (String)  |              jdbc:h2:mem:demo              |              Database URL              |
|    DB_DRIVER    | Java class (String) |               org.h2.Driver                |            Database driver             |
|   DB_USERNAME   |       String        |                     sa                     |           Database username            |
|   DB_PASSWORD   |       String        |                                            |           Database password            |
|  NASA_API_KEY   |       String        |                  DEMO_KEY                  |   NASA API key to call external API    |
| NASA_BROWSE_URL |    URL (String)     | http://www.neowsapp.com/rest/v1/neo/browse | External API (potentially can be used) |
|  NASA_FEED_URL  |    URL (String)     |   https://api.nasa.gov/neo/rest/v1/feed    | External API to request Asteroid data  |

_Notes:
<br/>- all configuration settings from above table are required. **DB**-settings can be changed, depending on your needs.
<br/>- **NASA-*** properties - constant (external dependency) static values.
<br/>- **DEMO_KEY** has much lower rate limits, so you’re encouraged to signup for your own API key
<br/>- own API key has an hourly limit: **1000** request per hour_

## API

Asteroids API provides only 2 endpoints, to serve incoming requests, according to specified features (check task
description for more details).

Common endpoints root: `/api/V1/asteroids`, default port is: `8080`

### 1. Closest to Earth 10 asteroids:
Example query:
```http request
GET http://localhost:8080/api/V1/asteroids?start_date=2022-07-01&end_date=2022-07-07
```
Query Parameters:

| Parameter  | Type | Format     | Required | Default |            Description            |
|:----------:|:----:|------------|----------|:-------:|:---------------------------------:|
| start_date | Date | YYYY-MM-DD | No       |  none   | Starting date for asteroid search |
|  end_date  | Date | YYYY-MM-DD | No       |  none   |  Ending date for asteroid search  |

_<br/>- maximum supported dates range - **7 days**, can be extended, in combination with cached & persisted data from the previous requests
<br/>- if no dates specified, NASA API defaults (7 days of current week) will be used to retrieve data
<br/>- at each request received data persist lo local (H2, in-memory, by default) database_


### 2. Asteroid details for specified year:

Example query:
```http request
GET http://localhost:8080/api/V1/asteroids/largest?year=2022
```
Query Parameters:

| Parameter | Type | Required | Default |             Description             |
|:---------:|:----:|----------|:-------:|:-----------------------------------:|
|   year    | int  | Yes      |  none   | Year to search asteroid details for |

_- as there is no specific NASA API endpoint to serve such type of request, previously persisted data will be used to filter and structure requested data_


## Summary

Asteroid API is build on top of [NASA Asteroids API](https://api.nasa.gov) - it's **Asteroids - NeoWs** (Near Earth Object Web Service).
**Neo - Feed** sub-service is only consumed, but consider to use **Neo - Browse** in the future - to mirror online data and become more independent of the external API.

Also, for simplicity - no sophisticated logic has been developed for caching data, just a common sense logic to store required data locally
to avoid extra calls to external API (and API key blocking). This logic can be improved with combination with key-value storage for better performance in case of high loads.

Due to lack of time some extra-features as (mentioned) cloud deployment and (not specified) wider testing (unit-, integration-), API specification (OpenAPI Specification), containerization, etc. were skipped, but considered in mind, for future improvements. The same stands for better business logic to overcome external-API limitations (7-days data feed range).
Still, there are variety of potential improvements and API extension:
- configurable request data filtering, sorting/ordering & pagination
- more flexibility and configurable request, with introducing new request parameters
- search (by name, id, ...etc.)
- more data (request, store and respond with)
- better/different internal data representation
- performance & load testing and API characteristics

But everything mentioned above and all extra features are followed by specification and (functional- and non-functional-) requirements, and can be implemented respectively.