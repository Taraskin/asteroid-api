package org.nasa.api;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nasa.api.domain.dto.api.AsteroidListResponseDto;
import org.nasa.api.domain.entity.Asteroid;
import org.nasa.api.repository.AsteroidRepository;
import org.nasa.api.service.AsteroidService;
import org.nasa.api.service.NasaApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.List;

@SpringBootTest
class AsteroidApiApplicationTests {

    @Autowired
    private NasaApiService service;

    @Autowired
    private AsteroidService asteroidService;

    @Autowired
    private AsteroidRepository asteroidRepository;


    @Test
    void contextLoads() {
    }

    @Test
    void testFeedDataLoad_no_dates() {
        List<AsteroidListResponseDto> feedResponseDto = service.fetchFeedData(null, null);
        Assertions.assertThat(feedResponseDto).isNotNull();
    }

    @Test
    void testFeedDataLoad_one_date() {
        List<AsteroidListResponseDto> feedResponseDto = service.fetchFeedData(LocalDate.now(), null);
        Assertions.assertThat(feedResponseDto).isNotNull();

        feedResponseDto = service.fetchFeedData(null, LocalDate.now());
        Assertions.assertThat(feedResponseDto).isNotNull();
    }


    @Test
    void testFeedDataLoad_date_range_out_of_max_days_limit() {
        LocalDate now = LocalDate.now();
        Assertions.assertThatThrownBy(() -> service.fetchFeedData(now.minusMonths(1L), now))
                .hasMessageContaining("Can't process request for provided date range: ");
    }

    @Test
    void testLocalDataLoad() {
        LocalDate startDate = LocalDate.parse("2022-07-01");
        LocalDate endDate = LocalDate.parse("2022-07-02");
        List<AsteroidListResponseDto> feedResponseDto = service.fetchFeedData(startDate, endDate);
        Assertions.assertThat(feedResponseDto).isNotNull();

        Asteroid asteroid = asteroidService.findAsteroidDetailsForYear(2022);
        Assertions.assertThat(asteroid).isNotNull();

        Asteroid kilometersMax = asteroidRepository.findAll(Sort.by(Sort.Direction.DESC, "diameterKilometersMax"))
                .listIterator().next();
        Assertions.assertThat(kilometersMax).isNotNull();

        Assertions.assertThat(asteroid).isEqualTo(kilometersMax);
    }
}
