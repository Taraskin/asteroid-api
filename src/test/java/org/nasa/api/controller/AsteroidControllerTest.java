package org.nasa.api.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.nasa.api.service.AsteroidService;
import org.nasa.api.service.DtoToAsteroidConverter;
import org.nasa.api.service.NasaApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.server.ResponseStatusException;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(value = AsteroidController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AsteroidControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AsteroidService asteroidService;

    @MockBean
    private NasaApiService nasaApiService;

    @MockBean
    private DtoToAsteroidConverter converter;

    @Test
    void testFindClosestAsteroids_exceptional_case() throws Exception {
        when(nasaApiService.fetchFeedData(eq(null), eq(null)))
                .thenThrow(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Something went wrong"));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/V1/asteroids")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.timestamp").isString())
                .andExpect(MockMvcResultMatchers.jsonPath("$.context").value("/api/V1/asteroids"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Something went wrong"))
        ;
    }
}