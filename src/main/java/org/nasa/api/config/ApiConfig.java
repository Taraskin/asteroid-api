package org.nasa.api.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.nasa.api.domain.dto.nasa.NasaApiErrorResponse;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.Optional;

@Slf4j
@Configuration
public class ApiConfig {

    @Bean
    public RestTemplateBuilder restTemplateBuilder() {
        return new RestTemplateBuilder();
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.errorHandler(
                        new ResponseErrorHandler() {
                            @Override
                            public boolean hasError(ClientHttpResponse response) throws IOException {
                                return response.getStatusCode().isError();
                            }

                            @Override
                            public void handleError(ClientHttpResponse response) throws IOException {
                                HttpStatus statusCode = response.getStatusCode();
                                if (statusCode.isError()) {
                                    String errorMessage = readErrorMessage(response).orElse(response.getStatusText());
                                    throw new ResponseStatusException(statusCode, errorMessage);
                                }
                            }

                            private Optional<String> readErrorMessage(ClientHttpResponse response) {
                                try {
                                    return Optional.ofNullable(
                                            new ObjectMapper().createParser(response.getBody())
                                                    .readValueAs(NasaApiErrorResponse.class).getErrorMessage()
                                    );
                                } catch (Exception e) {
                                    log.error("<NASA API> failed to parse Error: {}", e.getMessage(), e);
                                    return Optional.empty();
                                }
                            }
                        })
                .build();
    }
}
