package org.nasa.api.util;

import org.springframework.http.HttpHeaders;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;
import java.util.function.Function;

public class ApiUtils {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static <T> T getHeaderValue(HttpHeaders responseEntityHeaders, String key, Function<? super String, T> mapper, T defaultValue) {
        return Optional.ofNullable(responseEntityHeaders.get(key))
                .map(List::listIterator)
                .map(ListIterator::next)
                .map(mapper)
                .orElse(defaultValue);
    }

    public static Optional<String> localDateToString(LocalDate date) {
        return Optional.ofNullable(date).map(DATE_TIME_FORMATTER::format);
    }
}
