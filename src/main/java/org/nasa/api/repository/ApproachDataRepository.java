package org.nasa.api.repository;

import org.nasa.api.domain.entity.ApproachData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface ApproachDataRepository extends JpaRepository<ApproachData, String> {
    Set<ApproachData> findAllByDateBetween(String startDate, String endDate);
}
