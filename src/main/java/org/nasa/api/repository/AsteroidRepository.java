package org.nasa.api.repository;

import org.nasa.api.domain.entity.Asteroid;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AsteroidRepository extends JpaRepository<Asteroid, String> {

    Optional<Asteroid> findFirstByApproachData_DateBetweenOrderByDiameterKilometersMaxDesc(String startDate, String endDate);

    List<Asteroid> findAllByApproachData_DateBetween(String startDate, String endDate);
}

