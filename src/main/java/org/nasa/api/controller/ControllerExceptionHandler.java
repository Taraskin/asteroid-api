package org.nasa.api.controller;

import lombok.extern.slf4j.Slf4j;
import org.nasa.api.domain.dto.api.ErrorResponseDto;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.nio.file.AccessDeniedException;

@Slf4j
@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Customize default exception handler
     * @param ex - exception
     * @param body - request body
     * @param headers - request headers
     * @param status - request status
     * @param request - incoming request
     * @return - error response of common type - {@link ErrorResponseDto}
     */
    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex,
                                                             @Nullable Object body,
                                                             HttpHeaders headers,
                                                             HttpStatus status,
                                                             WebRequest request) {
        return handleException(ex, body, headers, status, request);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST) // 400
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {
        return handleException(ex, null, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ResponseStatus(HttpStatus.FORBIDDEN) // 403
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex, WebRequest request) {
        return handleException(ex, null, new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }

    @ExceptionHandler(HttpClientErrorException.class)
    public @ResponseBody
    ResponseEntity<Object> handleNotFound(HttpClientErrorException e, WebRequest request) {
        return handleException(e, e.getMessage(), new HttpHeaders(), e.getStatusCode(), request);
    }

    @ExceptionHandler(ResponseStatusException.class)
    public @ResponseBody
    ResponseEntity<Object> handleResponseStatusException(ResponseStatusException ex, WebRequest request) {
        return handleException(ex, ex.getReason(), new HttpHeaders(), ex.getStatus(), request);
    }

    private ResponseEntity<Object> handleException(Throwable e, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String errorMessage = body instanceof String ? (String) body : e.getMessage();
        String context = ((ServletWebRequest) request).getRequest().getRequestURI();
        ErrorResponseDto response = new ErrorResponseDto(context, errorMessage);
        return new ResponseEntity<>(response, headers, status);
    }
}
