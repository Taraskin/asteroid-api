package org.nasa.api.controller;

import lombok.extern.slf4j.Slf4j;
import org.nasa.api.domain.dto.api.AsteroidDetailsResponseDto;
import org.nasa.api.domain.dto.api.AsteroidListResponseDto;
import org.nasa.api.domain.entity.Asteroid;
import org.nasa.api.service.AsteroidService;
import org.nasa.api.service.NasaApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/V1/asteroids")
public class AsteroidController {

    private final NasaApiService nasaApiService;
    private final AsteroidService service;


    @Autowired
    public AsteroidController(AsteroidService service, NasaApiService nasaApiService) {
        this.nasaApiService = nasaApiService;
        this.service = service;
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<List<AsteroidListResponseDto>> findClosestAsteroids(
            @RequestParam(value = "start_date", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
            @RequestParam(value = "end_date", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate) {

        List<AsteroidListResponseDto> responseDtoList = nasaApiService.fetchFeedData(startDate, endDate);
        return ResponseEntity.ok(responseDtoList);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "largest", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<AsteroidDetailsResponseDto> findAsteroidDetailsForYear(@RequestParam(value = "year") int year) {
        Asteroid asteroidDetailsForYear = service.findAsteroidDetailsForYear(year);
        AsteroidDetailsResponseDto responseDto = AsteroidDetailsResponseDto.from(asteroidDetailsForYear);
        return ResponseEntity.ok(responseDto);
    }
}
