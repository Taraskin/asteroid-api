package org.nasa.api.domain.dto.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import org.nasa.api.domain.dto.nasa.DiameterDto;
import org.nasa.api.domain.entity.Asteroid;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AsteroidDetailsResponseDto {
    private String id;
    private String name;
    private DiameterDto kilometers;
    private DiameterDto meters;
    private DiameterDto miles;
    private DiameterDto feet;

    public static AsteroidDetailsResponseDto from(Asteroid asteroid) {
        return AsteroidDetailsResponseDto.builder()
                .id(asteroid.getId())
                .name(asteroid.getName())
                .kilometers(new DiameterDto(asteroid.getDiameterKilometersMin(), asteroid.getDiameterKilometersMax()))
                .meters(new DiameterDto(asteroid.getDiameterKilometersMin(), asteroid.getDiameterKilometersMax()))
                .miles(new DiameterDto(asteroid.getDiameterKilometersMin(), asteroid.getDiameterKilometersMax()))
                .feet(new DiameterDto(asteroid.getDiameterKilometersMin(), asteroid.getDiameterKilometersMax()))
                .build();
    }
}
