package org.nasa.api.domain.dto.nasa;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApproachDataDto {

    @JsonProperty("close_approach_date")
    private String date;

    @JsonProperty("epoch_date_close_approach")
    private Long epochDateTime;

    @JsonProperty("miss_distance")
    private MissDistanceDto distanceDto;

}
