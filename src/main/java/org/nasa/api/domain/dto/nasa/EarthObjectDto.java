package org.nasa.api.domain.dto.nasa;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EarthObjectDto {

    private String id;

    @JsonProperty("neo_reference_id")
    private String referenceId;

    private String name;

    @JsonProperty("nasa_jpl_url")
    private String nasaJplUrl;

    @JsonProperty("is_potentially_hazardous_asteroid")
    private boolean isHazardous;

    @JsonProperty("estimated_diameter")
    private EstimatedDiameterDto diameter;

    @JsonProperty("close_approach_data")
    private List<ApproachDataDto> approachDataList;
}
