package org.nasa.api.domain.dto.nasa;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EstimatedDiameterDto {
    private DiameterDto kilometers;
    private DiameterDto meters;
    private DiameterDto miles;
    private DiameterDto feet;
}
