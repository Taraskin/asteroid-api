package org.nasa.api.domain.dto.nasa;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.http.HttpStatus;


@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NasaApiErrorResponse {
    private int code;
    @JsonProperty("http_error")
    private HttpStatus status;
    @JsonProperty("error_message")
    private String errorMessage;
    private String request;
}
