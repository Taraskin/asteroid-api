package org.nasa.api.domain.dto.nasa;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FeedResponseDto {

    private LinksDto links;

    @JsonProperty("element_count")
    int elementCount;

    @JsonProperty("near_earth_objects")
    Map<String, List<EarthObjectDto>> nearEarthObjectsMap;
}
