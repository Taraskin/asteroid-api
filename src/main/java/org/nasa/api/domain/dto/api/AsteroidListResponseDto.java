package org.nasa.api.domain.dto.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import org.nasa.api.domain.dto.nasa.ApproachDataDto;
import org.nasa.api.domain.dto.nasa.EarthObjectDto;
import org.nasa.api.domain.dto.nasa.MissDistanceDto;
import org.nasa.api.domain.entity.Asteroid;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AsteroidListResponseDto {
    private String id;
    private String name;
    private MissDistanceDto distance;

    public static AsteroidListResponseDto from(EarthObjectDto dto) {
        MissDistanceDto distance = dto.getApproachDataList().stream()
                .map(ApproachDataDto::getDistanceDto)
                .min(MissDistanceDto::compare)
                .orElse(null);

        return AsteroidListResponseDto.builder()
                .id(dto.getId())
                .name(dto.getName())
                .distance(distance)
                .build();
    }

    public static AsteroidListResponseDto from(Asteroid asteroid) {
        MissDistanceDto distance = new MissDistanceDto(
                asteroid.getApproachData().getAstronomical(),
                asteroid.getApproachData().getLunar(),
                asteroid.getApproachData().getKilometers(),
                asteroid.getApproachData().getMiles()
        );
        return AsteroidListResponseDto.builder()
                .id(asteroid.getId())
                .name(asteroid.getName())
                .distance(distance)
                .build();
    }

    public static int compare(AsteroidListResponseDto o1, AsteroidListResponseDto o2) {
        return o1.getDistance().getKilometers().compareTo(o2.getDistance().getKilometers());
    }
}
