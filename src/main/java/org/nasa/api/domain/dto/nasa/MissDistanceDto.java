package org.nasa.api.domain.dto.nasa;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MissDistanceDto {
    private Double astronomical;
    private Double lunar;
    private Double kilometers;
    private Double miles;

    public static int compare(MissDistanceDto o1, MissDistanceDto o2) {
        return o1.getKilometers().compareTo(o2.getKilometers());
    }
}
