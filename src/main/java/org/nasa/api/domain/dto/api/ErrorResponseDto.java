package org.nasa.api.domain.dto.api;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;


@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponseDto {
    private Date timestamp;
    private String context;
    private String message;

    public ErrorResponseDto(String context, String message) {
        this.message = message;
        this.timestamp = new Date();
        this.context = context;
    }
}
