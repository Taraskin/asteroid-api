package org.nasa.api.domain.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Data
@Entity
public class Asteroid {
    @Id
    private String id;
    private String referenceId;
    private String name;
    private String nasaJplUrl;
    private boolean isHazardous;

    private Double diameterKilometersMin;
    private Double diameterKilometersMax;
    private Double diameterMetersMin;
    private Double diameterMetersMax;
    private Double diameterMilesMin;
    private Double diameterMilesMax;
    private Double diameterFeetMin;
    private Double diameterFeetMax;

    @ManyToOne
    private ApproachData approachData;
}
