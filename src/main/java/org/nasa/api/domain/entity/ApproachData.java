package org.nasa.api.domain.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class ApproachData {
    @Id
    private String date;
    private Long epochDateTime;
    private double astronomical;
    private double lunar;
    private double kilometers;
    private double miles;
}
