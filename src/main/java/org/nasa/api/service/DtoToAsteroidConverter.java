package org.nasa.api.service;

import lombok.extern.slf4j.Slf4j;
import org.nasa.api.domain.dto.nasa.ApproachDataDto;
import org.nasa.api.domain.dto.nasa.EarthObjectDto;
import org.nasa.api.domain.entity.ApproachData;
import org.nasa.api.domain.entity.Asteroid;
import org.nasa.api.repository.ApproachDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class DtoToAsteroidConverter implements Converter<EarthObjectDto, Asteroid> {

    private final ApproachDataRepository repository;

    @Autowired
    public DtoToAsteroidConverter(ApproachDataRepository repository) {
        this.repository = repository;
    }

    @Override
    public Asteroid convert(EarthObjectDto source) {
        Asteroid asteroid = new Asteroid();
        asteroid.setId(source.getId());
        asteroid.setReferenceId(source.getReferenceId());
        asteroid.setName(source.getName());
        asteroid.setNasaJplUrl(source.getNasaJplUrl());
        asteroid.setHazardous(source.isHazardous());
        // TODO: NPE prevention...
        asteroid.setDiameterKilometersMin(source.getDiameter().getKilometers().getMin());
        asteroid.setDiameterKilometersMax(source.getDiameter().getKilometers().getMax());
        asteroid.setDiameterMetersMin(source.getDiameter().getMeters().getMin());
        asteroid.setDiameterMetersMax(source.getDiameter().getMeters().getMax());
        asteroid.setDiameterMilesMin(source.getDiameter().getMiles().getMin());
        asteroid.setDiameterMilesMax(source.getDiameter().getMiles().getMax());
        asteroid.setDiameterFeetMin(source.getDiameter().getFeet().getMin());
        asteroid.setDiameterFeetMax(source.getDiameter().getFeet().getMax());

        ApproachDataDto dto = source.getApproachDataList().listIterator().next();
        ApproachData appData = new ApproachData();
        appData.setDate(dto.getDate());
        appData.setEpochDateTime(dto.getEpochDateTime());
        appData.setAstronomical(dto.getDistanceDto().getAstronomical());
        appData.setKilometers(dto.getDistanceDto().getKilometers());
        appData.setLunar(dto.getDistanceDto().getLunar());
        appData.setMiles(dto.getDistanceDto().getMiles());

        ApproachData data = repository.save(appData);
        asteroid.setApproachData(data);
        return asteroid;
    }
}
