package org.nasa.api.service;

import lombok.extern.slf4j.Slf4j;
import org.nasa.api.domain.dto.api.AsteroidListResponseDto;
import org.nasa.api.domain.dto.nasa.FeedResponseDto;
import org.nasa.api.domain.entity.Asteroid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.nasa.api.util.ApiUtils.getHeaderValue;
import static org.nasa.api.util.ApiUtils.localDateToString;

@Slf4j
@Service
public class NasaApiService {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private final DtoToAsteroidConverter converter;
    private final AsteroidService asteroidService;
    private final RestTemplate restTemplate;

    @Value("${min-key-remain-limit:10}")
    public int minimalKeyRemainingLimit;

    @Value("${asteroids-limit:10}")
    public int asteroidsLimit;

    @Value("${max-days-limit:7}")
    public int daysLimit;

    @Value("${API_KEY:DEMO_KEY}")
    private String apiKey;

    @Value("${FEED_URL:https://api.nasa.gov/neo/rest/v1/feed}")
    private String feedUrl;

    @Autowired
    public NasaApiService(DtoToAsteroidConverter converter, AsteroidService asteroidService, RestTemplate restTemplate) {
        this.converter = converter;
        this.asteroidService = asteroidService;
        this.restTemplate = restTemplate;
    }

    public List<AsteroidListResponseDto> fetchFeedData(LocalDate startDate, LocalDate endDate) {
        if (startDate != null && endDate != null) {
            // Check only if 2 dates are present

            if (isRangeOutOfMaxDaysLimit(startDate, endDate)) {
                String sDate = localDateToString(startDate).orElseThrow(() -> new IllegalArgumentException("Incorrect startDate: " + startDate));
                String eDate = localDateToString(endDate).orElseThrow(() -> new IllegalArgumentException("Incorrect startDate: " + endDate));

                List<String> rangeDates = Stream.iterate(
                                startDate,
                                d -> d.isBefore(endDate.plusDays(1)),
                                d -> d.plusDays(1))
                        .map(localDate -> localDate.format(DATE_TIME_FORMATTER))
                        .collect(Collectors.toList());

                List<String> cachedDatesInDb = asteroidService.findAllApproachDataRecordsBetween(startDate, endDate);
                if (!cachedDatesInDb.isEmpty()) {
                    rangeDates.removeAll(cachedDatesInDb);
                }
                // Read persisted data frm the database:
                log.info("Reading asteroids data from database... from={} to={}", sDate, eDate);
                List<Asteroid> asteroids = asteroidService.findAllAsteroidsBetweenDates(startDate, endDate);
                log.info("Found {} asteroids for date range [{} , {}]", asteroids.size(), sDate, eDate);

                if (!rangeDates.isEmpty()) {
                    // There are still some data, to addition to the persisted, that has to be fetched from the NASA API, if possible:
                    String newStartDateString = rangeDates.listIterator().next();
                    String newEndDateString = rangeDates.listIterator(rangeDates.size() - 1).next();

                    LocalDate newStartDay = LocalDate.parse(newStartDateString);
                    LocalDate newEndDay = LocalDate.parse(newEndDateString);

                    if (isRangeOutOfMaxDaysLimit(newStartDay, newEndDay)) {
                        // Provided date range with persisted data adjustment is still out of supported NASA API limit (7 days)
                        String errorMessage = String.format("Can't process request for provided date range: [%s ... %s]",
                                startDate, endDate);
                        log.error(errorMessage);
                        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorMessage);
                    }
                    // Fetch & persist piece of data from the NASA API:
                    fetchData(newStartDay, endDate);

                    // Read updated data from the database
                    asteroids = asteroidService.findAllAsteroidsBetweenDates(startDate, endDate);
                }
                return mapToResponse(asteroids);
            }
        }
        FeedResponseDto data = fetchData(startDate, endDate);
        return mapToResponse(data);
    }

    private FeedResponseDto fetchData(LocalDate startDate, LocalDate endDate) {
        MultiValueMap<String, String> uriVariables = new LinkedMultiValueMap<>();
        uriVariables.add("api_key", apiKey);
        localDateToString(startDate).ifPresent(s -> uriVariables.add("start_date", s));
        localDateToString(endDate).ifPresent(s -> uriVariables.add("end_date", s));

        UriComponents uriComponents = UriComponentsBuilder.fromUriString(feedUrl).queryParams(uriVariables).build().encode();

        ResponseEntity<FeedResponseDto> responseEntity = restTemplate.getForEntity(uriComponents.toUri(), FeedResponseDto.class);

        HttpHeaders responseEntityHeaders = responseEntity.getHeaders();
        log.debug("Response code: {}\nResponse headers:\n{}", responseEntity.getStatusCode(), responseEntityHeaders);

        Integer limit = getHeaderValue(responseEntityHeaders, "X-RateLimit-Limit", Integer::parseInt, 0);
        Integer remaining = getHeaderValue(responseEntityHeaders, "X-RateLimit-Remaining", Integer::parseInt, 0);

        if (minimalKeyRemainingLimit >= remaining) {
            log.info("WARN - approaching key limit!!!\nX-RateLimit-Limit = {}\nX-RateLimit-Remaining = {}", limit, remaining);
        }

        FeedResponseDto body = Optional.ofNullable(responseEntity.getBody()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No data received")
        );

        body.getNearEarthObjectsMap().forEach((s, earthObjectDtoList) -> {
            Set<Asteroid> asteroids = earthObjectDtoList.stream().map(converter::convert).collect(Collectors.toSet());
            asteroidService.persistAsteroidsInfo(asteroids);
        });

        log.debug("\nResponse body:\n{}", body);
        return body;
    }

    private List<AsteroidListResponseDto> mapToResponse(FeedResponseDto dto) {
        return Objects.requireNonNull(dto.getNearEarthObjectsMap(), "No data").values().stream()
                .flatMap(Collection::stream)
                .map(AsteroidListResponseDto::from)
                .filter(responseDto -> Objects.nonNull(responseDto.getDistance()))
                .sorted(AsteroidListResponseDto::compare)
                .limit(asteroidsLimit)
                .collect(Collectors.toList());
    }

    private List<AsteroidListResponseDto> mapToResponse(List<Asteroid> asteroids) {
        return asteroids.stream()
                .map(AsteroidListResponseDto::from)
                .filter(responseDto -> Objects.nonNull(responseDto.getDistance()))
                .sorted(AsteroidListResponseDto::compare)
                .limit(asteroidsLimit)
                .collect(Collectors.toList());
    }

    private boolean isRangeOutOfMaxDaysLimit(LocalDate startDate, LocalDate endDate) {
        return Duration.between(startDate.atStartOfDay(), endDate.atStartOfDay()).toDays() > daysLimit;
    }
}
