package org.nasa.api.service;

import lombok.extern.slf4j.Slf4j;
import org.nasa.api.domain.entity.ApproachData;
import org.nasa.api.domain.entity.Asteroid;
import org.nasa.api.repository.ApproachDataRepository;
import org.nasa.api.repository.AsteroidRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nasa.api.util.ApiUtils.localDateToString;

@Slf4j
@Service
public class AsteroidService {

    private final AsteroidRepository asteroidRepository;
    private final ApproachDataRepository approachDataRepository;

    @Autowired
    public AsteroidService(AsteroidRepository asteroidRepository, ApproachDataRepository approachDataRepository) {
        this.asteroidRepository = asteroidRepository;
        this.approachDataRepository = approachDataRepository;
    }

    public Asteroid findAsteroidDetailsForYear(int year) {
        if (year <= 0) {
            log.error("AsteroidService::findAsteroidDetailsForYear - incorrect [year] parameter - {}", year);
            throw new IllegalArgumentException("Incorrect [year] parameter: " + year);
        }
        String s1 = LocalDate.of(year, Month.JANUARY, 1).toString();
        String s2 = LocalDate.of(year, Month.DECEMBER, 31).toString();

        return asteroidRepository
                .findFirstByApproachData_DateBetweenOrderByDiameterKilometersMaxDesc(s1, s2)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No data for selected year: " + year));
    }

    @Transactional
    public void persistAsteroidsInfo(Set<Asteroid> asteroids) {
        List<Asteroid> persistedAsteroids = asteroidRepository.saveAllAndFlush(asteroids);
        log.info("Persisted {} asteroids", persistedAsteroids.size());
    }

    public List<String> findAllApproachDataRecordsBetween(@NonNull LocalDate startDate, @NonNull LocalDate endDate) {
        String sDate = localDateToString(startDate).orElseThrow(() -> new IllegalArgumentException("Incorrect startDate: " + startDate));
        String eDate = localDateToString(endDate).orElseThrow(() -> new IllegalArgumentException("Incorrect startDate: " + endDate));
        List<String> datesInDb = approachDataRepository.findAllByDateBetween(sDate, eDate).stream()
                .map(ApproachData::getDate)
                .collect(Collectors.toList());
        log.info("Found {} cached data rows in database", datesInDb.size());
        return datesInDb;
    }

    public List<Asteroid> findAllAsteroidsBetweenDates(@NonNull LocalDate startDate, @NonNull LocalDate endDate) {
        String sDate = localDateToString(startDate).orElseThrow(() -> new IllegalArgumentException("Incorrect startDate: " + startDate));
        String eDate = localDateToString(endDate).orElseThrow(() -> new IllegalArgumentException("Incorrect startDate: " + endDate));
        List<Asteroid> asteroids = asteroidRepository.findAllByApproachData_DateBetween(sDate, eDate);
        log.info("Found {} cached asteroids data rows in database", asteroids.size());
        return asteroids;
    }
}
